
package graph;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import snmp.Data;
import snmp.SNMPController;

public class GraphPlotter extends Application 
{
	
	final int WINDOW_SIZE = 6;
	private ScheduledExecutorService scheduledExecutorService;
	private long cnt = 0;
	
	private final String[] targets = {"192.168.10.1","192.168.20.1","192.168.30.1"};
	private SNMPController agent1 = new SNMPController(targets[0], 161, "si2019");
	private SNMPController agent2 = new SNMPController(targets[1], 161, "si2019");
	private SNMPController agent3 = new SNMPController(targets[2], 161, "si2019");
	private ArrayList<Data> dataList = new ArrayList<Data>();
	
	//inOctaveParams
	private ArrayList<XYChart.Series<Number, Number>> inOctaveData = new ArrayList<XYChart.Series<Number,Number>>();
	private ArrayList<Long> inOctaveLastVal = new ArrayList<Long>();
	
	//outOctaveParams
	private ArrayList<XYChart.Series<Number, Number>> outOctaveData = new ArrayList<XYChart.Series<Number,Number>>();
	private ArrayList<Long> outOctaveLastVal = new ArrayList<Long>();
	
	//inPktsParams
	private ArrayList<XYChart.Series<Number, Number>> inPktsData = new ArrayList<XYChart.Series<Number,Number>>();
	//private ArrayList<Long> inPktsLastVal = new ArrayList<Long>();
	
	//outPktsParams
	private ArrayList<XYChart.Series<Number, Number>> outPktsData = new ArrayList<XYChart.Series<Number,Number>>();
	//private ArrayList<Long> outPktsLastVal = new ArrayList<Long>();
	
	@Override
	public void start(Stage primaryStage)
	{
		init(primaryStage);
	}
	
	private void init(Stage primaryStage)
	{
		HBox throughput = new HBox();
		HBox packages = new HBox();
		
		Scene scene = new Scene(throughput,800,600);
		primaryStage.setTitle("Protok podataka");
		primaryStage.setScene(scene);
		primaryStage.show();

		Stage secondStage = new Stage();
		secondStage.setTitle("Broj paketa");
		Scene scene2 = new Scene(packages, 800,600);
		secondStage.setScene(scene2);
		secondStage.show();
		
		//xAxes
		NumberAxis xInOctAxis = new NumberAxis(0,50,10);
		xInOctAxis.setLabel("Vreme[s]");
		xInOctAxis.setAutoRanging(false);
		NumberAxis xOutOctAxis = new NumberAxis(0,50,10);
		xOutOctAxis.setLabel("Vreme[s]");
		xOutOctAxis.setAutoRanging(false);
		NumberAxis xInPktsAxis = new NumberAxis(0,50,10);
		xInPktsAxis.setLabel("Vreme[s]");
		xInPktsAxis.setAutoRanging(false);
		NumberAxis xOutPktsAxis = new NumberAxis(0,50,10);
		xOutPktsAxis.setLabel("Vreme[s]");
		xOutPktsAxis.setAutoRanging(false);
		
		//yAxes
		NumberAxis yInOctAxis = new NumberAxis();
		yInOctAxis.setLabel("Ulazni protok[bit]");
		NumberAxis yOutOctAxis = new NumberAxis();
		yOutOctAxis.setLabel("Izlazni protok[bit]");
		NumberAxis yInPktsAxis = new NumberAxis();
		yInPktsAxis.setLabel("Broj ulaznih paketa");
		NumberAxis yOutPktsAxis = new NumberAxis();
		yOutPktsAxis.setLabel("Broj izlaznih paketa");
		
		//lineCharts
		LineChart<Number,Number> inOctlineChart = new LineChart<Number,Number>(xInOctAxis,yInOctAxis);
		inOctlineChart.setTitle("InOctLineChart");
		LineChart<Number,Number> outOctlineChart = new LineChart<Number,Number>(xOutOctAxis,yOutOctAxis);
		outOctlineChart.setTitle("outOctLineChart");
		//outOctlineChart.setLegendVisible(false);
		LineChart<Number,Number> inPktslineChart = new LineChart<Number,Number>(xInPktsAxis,yInPktsAxis);
		inOctlineChart.setTitle("InOctLineChart");
		LineChart<Number,Number> outPktslineChart = new LineChart<Number,Number>(xOutPktsAxis,yOutPktsAxis);
		outOctlineChart.setTitle("outOctLineChart");
		//outOctlineChart.setLegendVisible(false);
		
		dataList.addAll(agent1.getData());
		dataList.addAll(agent2.getData());
		dataList.addAll(agent3.getData());
		
		for(int i = 0; i < dataList.size(); i++)
		{
			XYChart.Series<Number, Number> d = new XYChart.Series<Number,Number>();
			d.setName(dataList.get(i).getIp());
			d.getData().add(new XYChart.Data<Number,Number>(cnt, 0));
			inOctaveData.add(d);
			inOctaveLastVal.add(dataList.get(i).getInOctave());
			inOctlineChart.getData().add(d);
			
			d = new XYChart.Series<Number,Number>();
			d.setName(dataList.get(i).getIp());
			d.getData().add(new XYChart.Data<Number,Number>(cnt, 0));
			outOctaveData.add(d);
			outOctaveLastVal.add(dataList.get(i).getOutOctave());
			outOctlineChart.getData().add(d);
			
			d = new XYChart.Series<Number,Number>();
			d.setName(dataList.get(i).getIp());
			d.getData().add(new XYChart.Data<Number,Number>(cnt, 0));
			inPktsData.add(d);
			//inPktsLastVal.add(dataList.get(i).getInPkts());
			inPktslineChart.getData().add(d);
			
			d = new XYChart.Series<Number,Number>();
			d.setName(dataList.get(i).getIp());
			d.getData().add(new XYChart.Data<Number,Number>(cnt, 0));
			outPktsData.add(d);
			//outPktsLastVal.add(dataList.get(i).getOutPkts());
			outPktslineChart.getData().add(d);
		}
			
		throughput.getChildren().add(inOctlineChart);
		throughput.getChildren().add(outOctlineChart);
		
		packages.getChildren().add(inPktslineChart);
		packages.getChildren().add(outPktslineChart);
		
		scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
		
		scheduledExecutorService.scheduleAtFixedRate(()->
		{
			dataList.clear();
			dataList.addAll(agent1.getData());
			dataList.addAll(agent2.getData());
			dataList.addAll(agent3.getData());
			
			//System.out.println("Novo stanje: ");
			for(int i = 0; i < dataList.size(); i++)
			{
				XYChart.Series<Number, Number> d = inOctaveData.get(i);
				//System.out.println(d.getData());
				Long currInOctState = dataList.get(i).getInOctave();
				//System.out.println("currState = " + currInOctState);
				Long prevInOctState = inOctaveLastVal.get(i);
				//System.out.println("lastState = " + prevInOctState);
				//System.out.println("Tr stanje:  " + Math.abs(currInOctState - prevInOctState));
				inOctaveLastVal.set(i, currInOctState);
				d.getData().add(new XYChart.Data<Number,Number>(cnt, (0.8*(currInOctState - prevInOctState))));
				if (d.getData().size()>10)
					d.getData().remove(0);
				//lineChart.getData().add(d);
				
				d = outOctaveData.get(i);
				//System.out.println(d.getData());
				Long currOutOctState = dataList.get(i).getOutOctave();
				//System.out.println("currState = " + currOutOctState);
				Long prevOutOctState = outOctaveLastVal.get(i);
				//System.out.println("lastState = " + prevOutOctState);
				//System.out.println("Tr stanje:  " + Math.abs(currOutOctState - prevOutOctState));
				outOctaveLastVal.set(i, currOutOctState);
				d.getData().add(new XYChart.Data<Number,Number>(cnt, (0.8*(currOutOctState - prevOutOctState))));
				if (d.getData().size()>10)
					d.getData().remove(0);
				//lineChart.getData().add(d);
			
				d = inPktsData.get(i);
				Long currInPkts = dataList.get(i).getInPkts();
				d.getData().add(new XYChart.Data<Number,Number>(cnt, currInPkts));
				if (d.getData().size()>10)
					d.getData().remove(0);
				
				d = outPktsData.get(i);
				Long currOutPkts = dataList.get(i).getOutPkts();
				d.getData().add(new XYChart.Data<Number,Number>(cnt, currOutPkts));
				if (d.getData().size()>10)
					d.getData().remove(0);
				
			}
			cnt+=10;
			xInOctAxis.setUpperBound(cnt>50? cnt-10 : 50);
			xInOctAxis.setLowerBound(cnt>50? cnt-60 : 0);
			xInOctAxis.setTickUnit(10);
			xOutOctAxis.setUpperBound(cnt>50? cnt-10 : 50);
			xOutOctAxis.setLowerBound(cnt>50? cnt-60 : 0);
			xOutOctAxis.setTickUnit(10);
			
			xInPktsAxis.setUpperBound(cnt>50? cnt-10 : 50);
			xInPktsAxis.setLowerBound(cnt>50? cnt-60 : 0);
			xInPktsAxis.setTickUnit(10);
			xOutPktsAxis.setUpperBound(cnt>50? cnt-10 : 50);
			xOutPktsAxis.setLowerBound(cnt>50? cnt-60 : 0);
			xOutPktsAxis.setTickUnit(10);
		}
		, 5, 10, TimeUnit.SECONDS);
	}	
	
	@Override
	public void stop() throws Exception
	{
		super.stop();
		scheduledExecutorService.shutdownNow();
	}
	
	public static void main(String[] args)
	{
		launch(args);
	}
	
}

//https://openjfx.io/openjfx-docs/#install-javafx
//sajt: https://wiki.eclipse.org/Efxclipse/Tutorials/AddingE(fx)clipse_to_eclipse#Installing_e.28fx.29clipse_IDE.

/*
--module-path /home/korisnik/Desktop/javafx-sdk-11.0.2/lib --add-modules javafx.controls,javafx.fxml
*/
