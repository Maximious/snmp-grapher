package snmp;

public class Data 
{
	private String macAddress;
	private String ipAddress;
	private long inOctave = 0;
	private long outOctave = 0;
	private long inPkts = 0;
	private long outPkts = 0;
	
	public Data(String mac, String ip)
	{
		macAddress = mac;
		ipAddress = ip; 
	}
	
	
	public long getInPkts() {
		return inPkts;
	}



	public void setInPkts(long inPkts) {
		this.inPkts = inPkts;
	}



	public long getOutPkts() {
		return outPkts;
	}

	public void setOutPkts(long outPkts) {
		this.outPkts = outPkts;
	}



	public long getInOctave() {
		return inOctave;
	}

	public void setInOctave(long inOctave) {
		this.inOctave = inOctave;
	}

	public long getOutOctave() {
		return outOctave;
	}

	public void setOutOctave(long outOctave) {
		this.outOctave = outOctave;
	}
	
	public String getIp()
	{
		return ipAddress;
	}
	
	public String getMac()
	{
		return macAddress;
	}
	
	public String toString()
	{
		return ipAddress + "  " + macAddress + 
				"   " + inOctave + "   " + outOctave +
				"   " + inPkts + "   " + outPkts;
	}
	
}