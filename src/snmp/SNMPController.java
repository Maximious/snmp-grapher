package snmp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.*;
import com.ireasoning.protocol.snmp.SnmpConst;
import com.ireasoning.protocol.snmp.SnmpSession;
import com.ireasoning.protocol.snmp.SnmpTableModel;
import com.ireasoning.protocol.snmp.SnmpTarget;

public class SNMPController 
{
	private final int IN_OCT_COLUMN = 9;
	private final int OUT_OCT_COLUMN = 15;
	private final int IN_PKTS_UNI_COLUMN = 10;
	private final int OUT_PKTS_UNI_COLUMN = 16;
	private final int IN_PKTS_NUNI_COLUMN = 11;
	private final int OUT_PKTS_NUNI_COLUMN = 7;
	private final int MAC_COLUMN = 5;
//	private final int NAME_COLUMN = 1;
	private final int IP_COLUMN = 2;
	
	private SnmpSession session;
	private String ip,community;
	private int port;
	private final String STATIC = "4"; // static is 4 in table
	private ArrayList<Data> data;
	
	public SNMPController(String ip,int port,String community)
	{
		this.ip = ip;
		this.port = port;
		this.community = community;
		SnmpTarget target = new SnmpTarget(ip,port,community,community,SnmpConst.SNMPV2);
		try {
			session =  new SnmpSession(target);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	@SuppressWarnings("deprecation")
	@Override
	protected void finalize()
	{
		session.close();
		try {
			super.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	private void generateData()
	{
		//System.out.println("Generating data! ");
		data = new ArrayList<Data>();
		try {
			SnmpSession.loadMib("/home/korisnik/Downloads/ireasoning/mibbrowser/mibs/RFC1213-MIB");
			
			SnmpTableModel ntm = new SnmpTableModel();
			//System.out.print("Tabela je ucitana: ");
			//System.out.println(
			session.snmpGetTable("ipNetToMediaTable", ntm);
			//);
			
			//ispis tabele
			for (int i = 0; i < ntm.getRowCount(); i++)
			{
				if (ntm.getValueAt(i, 3).toString().equals(STATIC)) 
				{
					/*
					for (int j = 0; j < ntm.getColumnCount(); j++)
					{
						System.out.print(ntm.getValueAt(i, j) + "    ");
					}*/
					data.add(new Data(ntm.getValueAt(i, 1).toString(), //mac is here in 1 column
							ntm.getValueAt(i, IP_COLUMN).toString()));
					//System.out.println();
				}
			}
			
			SnmpTableModel tm = new SnmpTableModel();
	//		System.out.print("Tabela je ucitana: ");
		//	System.out.println(
			session.snmpGetTable("ifTable", tm);
			//);
			
			for (int i = 0; i < tm.getRowCount(); i++)
			{
				//String iName = tm.getValueAt(i, 1).toString();
				String mac = tm.getValueAt(i, MAC_COLUMN).toString();
				long inOctave =Integer.parseInt(tm.getValueAt(i, IN_OCT_COLUMN).toString());
				long outOctave = Integer.parseInt(tm.getValueAt(i, OUT_OCT_COLUMN).toString());
				long inUniPkts = Integer.parseInt(tm.getValueAt(i, IN_PKTS_UNI_COLUMN).toString());
				long inNUniPkts = Integer.parseInt(tm.getValueAt(i, IN_PKTS_NUNI_COLUMN).toString());
				long outUniPkts = Integer.parseInt(tm.getValueAt(i, OUT_PKTS_UNI_COLUMN).toString());
				long outNUniPkts = Integer.parseInt(tm.getValueAt(i, OUT_PKTS_NUNI_COLUMN).toString());
				//ystem.out.println(iName + " " + mac);

				/*
				if (iName.contains("Loopback"))
				{
					Data d = new Data("",iName);
					d.setInOctave(inOctave);
					d.setOutOctave(outOctave);
					data.add(d);
					//System.out.println("Loopback found");
					System.out.println(d);
					continue;
				}*/
				
				Stream<Data> dStream = data.stream();
				dStream.forEach(data ->  
				{
					//System.out.println(data);
					if (data.getMac().equals(mac))
					{
						data.setInOctave(inOctave);
						data.setOutOctave(outOctave);
						data.setInPkts(inUniPkts + inNUniPkts);
						data.setOutPkts(outUniPkts + outNUniPkts);
						//System.out.println(data);
					}
				});
			
				
			}		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public synchronized ArrayList<Data> getData()
	{
		if (data == null)
		{
			generateData();
		}
		else
		{
			updateData();
		}
		return data;
	}
	
	private void updateData()
	{
		//System.out.println("Updating data!");
		SnmpTableModel tm = new SnmpTableModel();
		try {
			session.snmpGetTable("ifTable", tm);
			
			for (int i = 0; i < tm.getRowCount(); i++)
			{
				String mac = tm.getValueAt(i, MAC_COLUMN).toString();
				long inOctave =Integer.parseInt(tm.getValueAt(i, IN_OCT_COLUMN).toString());
				long outOctave = Integer.parseInt(tm.getValueAt(i, OUT_OCT_COLUMN).toString());
				long inUniPkts = Integer.parseInt(tm.getValueAt(i, IN_PKTS_UNI_COLUMN).toString());
				long inNUniPkts = Integer.parseInt(tm.getValueAt(i, IN_PKTS_NUNI_COLUMN).toString());
				long outUniPkts = Integer.parseInt(tm.getValueAt(i, OUT_PKTS_UNI_COLUMN).toString());
				long outNUniPkts = Integer.parseInt(tm.getValueAt(i, OUT_PKTS_NUNI_COLUMN).toString());
				
				Stream<Data> dStream = data.stream();
				dStream.forEach(data ->  
				{
					if (data.getMac().equals(mac))
					{
						data.setInOctave(inOctave);
						data.setOutOctave(outOctave);
						data.setInPkts(inUniPkts + inNUniPkts);
						data.setOutPkts(outUniPkts + outNUniPkts);
						//System.out.println(data);
					}
				});
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args)
	{
		String[] targets = {"192.168.10.1","192.168.20.1","192.168.30.1"};
		SNMPController agent1 = new SNMPController(targets[0], 161, "si2019");
		SNMPController agent2 = new SNMPController(targets[1], 161, "si2019");
		SNMPController agent3 = new SNMPController(targets[2], 161, "si2019");
		System.out.println(agent1.getData()); 
		System.out.println(agent2.getData()); 
		System.out.println(agent3.getData()); 
	}
}